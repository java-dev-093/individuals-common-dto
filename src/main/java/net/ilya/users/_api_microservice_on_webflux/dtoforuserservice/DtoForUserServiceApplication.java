package net.ilya.users._api_microservice_on_webflux.dtoforuserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DtoForUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtoForUserServiceApplication.class, args);
    }

}
