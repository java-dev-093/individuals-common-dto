package net.ilya.users._api_microservice_on_webflux.dtoforuserservice.entity;

public enum StatusEntity {
    ACTIVE,
    UPDATED,
    DELETED,
}
